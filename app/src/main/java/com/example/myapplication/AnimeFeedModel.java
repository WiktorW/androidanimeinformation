package com.example.myapplication;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Collection;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AnimeFeedModel {
    public AnimeFeedData[] data;
}

@JsonIgnoreProperties(ignoreUnknown = true)
class AnimeFeedData {
    public Integer id;
    public String type;
    public AnimeFeedAttributes attributes;
}

@JsonIgnoreProperties(ignoreUnknown = true)
class AnimeFeedAttributes {
    public String synopsis;
    public String averageRating;
    public String startDate;
    public String endDate;
    public AnimeFeedTitles titles;
    public AnimeFeedPosterImage posterImage;
    public String episodeCount;
    public String episodeLength;
}

@JsonIgnoreProperties(ignoreUnknown = true)
class AnimeFeedTitles {
    public String en;
}

@JsonIgnoreProperties(ignoreUnknown = true)
class AnimeFeedPosterImage {
    public String medium;
    public String large;
}
