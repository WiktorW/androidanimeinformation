package com.example.myapplication;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button1 = findViewById(R.id.button);
        button1.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                try {
                    StringAnimeBuilder build = new StringAnimeBuilder();
                    TextView animeresult = findViewById(R.id.textView2);
                    AnimeClient animeClient = new AnimeClient();
                    TextView animename = findViewById(R.id.AnimeName);
                    AsyncTask<String, Void, AnimeFeedModel> result = animeClient.execute(animename.getText().toString());
                    animeresult.setText(build.SortedAnime(result));
                    GetImageFromUrl getImageFromUrl= new GetImageFromUrl();
                    ScrollView scrollView = findViewById(R.id.ScrollView);
                    AsyncTask<String,Void,Bitmap> animeImage = getImageFromUrl.execute(result.get().data[0].attributes.posterImage.medium);
                    Drawable d = new BitmapDrawable(getResources(), animeImage.get());
                    scrollView.setBackground(d);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
