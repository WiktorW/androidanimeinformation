package com.example.myapplication;

import android.os.AsyncTask;
import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class AnimeClient extends AsyncTask<String, Void, AnimeFeedModel> {
    String apiUrl = "https://kitsu.io/api/edge/anime";

    private String readStream(InputStream is) {
        try {
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            int i = is.read();
            while(i != -1) {
                bo.write(i);
                i = is.read();
            }
            return bo.toString();
        } catch (IOException e) {
            return "";
        }
    }

    @Override
    protected AnimeFeedModel doInBackground(String... names) {
        AnimeFeedModel result = null;
        URL url = null;
        try {
            url = new URL(apiUrl + "?filter[text]=" + names[0]);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        HttpURLConnection urlConnection = null;

        try {
            urlConnection = (HttpURLConnection) url.openConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String httpResult = readStream(in);
            ObjectMapper objectMapper = new ObjectMapper();
            result = objectMapper.readValue(httpResult, AnimeFeedModel.class);
        }
        catch (Exception ex) {
            return null;
        }
        finally {
            urlConnection.disconnect();
            return result;
        }
    }
}
