package com.example.myapplication;

import android.os.AsyncTask;

import java.util.concurrent.ExecutionException;

public class StringAnimeBuilder {
    public String  SortedAnime(AsyncTask<String, Void, AnimeFeedModel> result) throws ExecutionException, InterruptedException {
        String animeInfo = "Title: " + result.get().data[0].attributes.titles.en + "\n" + "Average Rating: "
                + result.get().data[0].attributes.averageRating
                + "\nStarted Airing: " + result.get().data[0].attributes.startDate + "\nFinished Airing: "
                + result.get().data[0].attributes.endDate + "\n" + "Episode Count: " +
                result.get().data[0].attributes.episodeCount + "\nEpisode Length: " +
                result.get().data[0].attributes.episodeLength + "\nSynopsis: "
                + result.get().data[0].attributes.synopsis;
        return animeInfo;
    }
}
